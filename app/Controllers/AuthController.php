<?php
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;

class AuthController extends BaseController
{    
    protected $userModel;
    /**
     * @csrfOff
     */
    public function login()
    {
        $session = session();
        $userModel = new UserModel();

        // Validation des données du formulaire
        $validationRules = [
            'user' => 'required',
            'password' => 'required'
        ];

        if ($this->validate($validationRules)) {
            // Récupérer les données soumises depuis le formulaire
            $email = $this->request->getPost('user');
            $password = $this->request->getPost('password');

            // Vérifier les informations d'identification de l'utilisateur
            $user = $userModel->where('user', $user)
                              ->first();

            if ($user && password_verify($password, $user['password'])) {
                // Authentification réussie, définir la session de l'utilisateur
                $session->set('id', $userId['id']);

                // Rediriger vers la page d'accueil
                return redirect()->to('/');
            } else {
                // Informer l'utilisateur que les informations d'identification sont incorrectes
                return $this->twig->render('login.html', ['error' => 'Invalid credentials']);
            }
        } else {
            // Afficher le formulaire de connexion avec les erreurs de validation
            return $this->twig->render('login.html', ['validation' => $this->validator]);
        }
    }

    public function logout()
    {
        $session = session();

        // Fermer la session de l'utilisateur
        $session->destroy();

        // Rediriger vers la page de connexion
        return redirect()->to('/login');
    }
}
