<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table = 'users'; // Nom de la table des utilisateurs dans la base de données
    protected $primaryKey = 'id'; // Nom de la clé primaire de la table

    protected $allowedFields = ['user', 'id', 'password']; // Champs autorisés à être modifiés par le modèle

    protected $returnType = 'array'; // Type de données retournées par les requêtes de ce modèle

    // Autres propriétés et méthodes...

    public function getUserById($userId)
    {
        // Méthode pour récupérer un utilisateur par son adresse id
        return $this->where('id', $userId)->first();

    }
 /**
     * Vérifie si le mot de passe fourni correspond à celui de l'utilisateur.
     *
     * @param array $userData Les données de l'utilisateur récupérées de la base de données.
     * @param string $password Le mot de passe fourni par l'utilisateur.
     * @return bool True si les mots de passe correspondent, sinon false.
     */
    public function verifyPassword($userData, $password)
    {
        // Récupérer le mot de passe haché de l'utilisateur
        $hashedPassword = $userData['password'];

        // Vérifier si le mot de passe fourni correspond au mot de passe haché
        return password_verify($password, $hashedPassword);
    }

    // Autres méthodes pour la gestion des utilisateurs...
}

