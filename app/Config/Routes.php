<?php

use CodeIgniter\Router\RouteCollection;
use App\Controllers\News; 
use App\Controllers\Pages;
use App\Controllers\AuthController;




/**
 * @var RouteCollection $routes
 */


$routes->get('/', 'Home::index');
$routes->get('/login', 'AuthController::login');
$routes->post('/login', 'Home::index');
